CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Image Lazyload module adds lazy load functionality to all image fields. 
An optional text format is provided to lazy load images added to text areas.
Lazy loading images can dramatically decrease initial page load times.

This module differs from other lazy load modules in that it doesn't depend on
jQuery.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/cneigh/2558559


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2558559

REQUIREMENTS
------------
This module requires the following modules:
 * Libraries (https://drupal.org/project/libraries)
 * LazyLoad (https://github.com/verlok/lazyload)

 INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


 * Install the LazyLoad javascript library to sites/all/libraries as you 
   would normally install a library.

CONFIGURATION
-------------
There is no required configuration. When enabled, the module will apply lazy
load functionality to all images added via an image field.

 * (Optional) Add Image Lazyload text format in
   Administration » Configuration » Content authoring » Text formats


   - Select the "Lazyload images" checkbox under Enabled filters to add
     lazyload functionality to images embedded in text areas.

TROUBLESHOOTING
---------------
 * If the "Lazyload images" filter is causing unexpected results, check
   the following:


   - Check the weight of the "Lazyload images" filter. It may be conflicting
     with another text format filter. Try altering the order of the filters.

MAINTAINERS
-----------
Current maintainers:
 * Chris Neigh (cneigh) - https://drupal.org/user/445658
