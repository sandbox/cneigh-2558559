; Make file for Art Websites.
api = 2
core = 7.x

libraries[lazyload][download][type] = "get"
libraries[lazyload][download][url] = "https://github.com/verlok/lazyload/archive/master.zip"
libraries[lazyload][directory_name] = "lazyload"
libraries[lazyload][destination] = "libraries"