/**
 * @file
 * Handles lazy load implementation.
 */

(function () {
  "use strict";

  Drupal.behaviors.image_lazyload = {

    // This behavior function is called when new element is added.
    attach: function (context, settings) {
      new LazyLoad();
    }

  };

})();
